def reverser
  words = yield.split(" ")
  words.each(&:reverse!)
  words.join(" ")
end

def adder(summand = 1)
  summand + yield
end

def repeater(n=1)
  n.times { yield }
end
