require "time"

def measure(n = 1)
  run_times = []

  n.times do
    before = Time.now
    yield
    after = Time.now
    run_times.push(after - before)
  end

  average(run_times)
end

def average(numbers)
  sum = 0.0
  numbers.each do |i|
    sum += i
  end
  sum / numbers.length
end
